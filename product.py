# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Equal, Eval, Not, In
from trytond.transaction import Transaction
from trytond.pool import Pool


class ProductEAV(ModelSQL, ModelView):
    'EAV Design for Product Attributes'
    _name = 'product.eav'
    _description = __doc__
    _rec_name = 'attribute'

    product = fields.Many2One('product.product', 'Product', required=True,
        ondelete='CASCADE')
    attribute = fields.Many2One('product.attribute', 'Attribute', required=True,
        on_change=['attribute'], ondelete='RESTRICT')
    type = fields.Char('Type')
    value = fields.Function(fields.Char('Value'), 'get_value')
    value_char = fields.Char('Value',
        states={
            'invisible': Not(In(Eval('type'), ['char','binary'])),
            'required': In(Eval('type'), ['char','binary']),
        }, on_change=['value_char'], depends=['value', 'type'])
    value_char_translate = fields.Char('Value',
        states={
            'invisible': Not(Equal(Eval('type'), 'char_translate')),
            'required': Equal(Eval('type'), 'char_translate'),
        }, on_change=['value_char_translate'], depends=['value', 'type'],
        translate=True)
    value_number = fields.Numeric('Value', digits=(16, Eval('digits', 2)),
        states={
            'invisible': Not(Equal(Eval('type'), 'number')),
            'required': Equal(Eval('type'), 'number'),
        }, on_change=['value_number', 'attribute'],
        depends=['value', 'type', 'digits'])
    value_binary_cache = fields.Binary('File',
        states={
            'invisible': Not(Equal(Eval('type'), 'binary')),
            'required': Equal(Eval('type'), 'binary'),
        }, depends=['value', 'type'])
    value_select = fields.Many2One('product.attribute.option', 'Value',
        states={
            'invisible': Not(Equal(Eval('type'), 'select')),
            'required': Equal(Eval('type'), 'select'),
        }, on_change=['value_select'], depends=['value', 'type', 'attribute',],
        domain=[('attribute', '=', Eval('attribute'))])
    digits = fields.Function(fields.Integer('Digits',
        on_change_with=['attribute']), 'get_digits')

    def on_change_attribute(self, vals):
        attribute_obj = Pool().get('product.attribute')
        res = {}
        if vals.get('attribute'):
            res['type'] = attribute_obj.read(vals['attribute'],
                ['value_type'])['value_type']
        return res

    def get_value(self, ids, name):
        res = {}
        for eav in self.browse(ids):
            value_type = eav.attribute.value_type
            if value_type == 'char':
                res[eav.id] = eav.value_char
            elif value_type == 'char_translate':
                res[eav.id] = eav.value_char_translate
            elif value_type == 'number':
                res[eav.id] = self._format_number(eav.attribute,
                    eav.value_number)
            elif value_type == 'binary':
                res[eav.id] = eav.value_char
            elif value_type == 'select':
                res[eav.id] = eav.value_select.rec_name
        return res

    def _format_number(self, attribute, value_number):
        lang_obj = Pool().get('ir.lang')
        lang_ids = lang_obj.search([
            ('code', '=', Transaction().context.get('language'))
            ], limit=1)
        lang = lang_obj.browse(lang_ids[0])

        fmt = '%.' + str(attribute.digits) + 'f'
        return lang_obj.format(lang, fmt, value_number)

    def get_digits(self, ids, name):
        res = {}
        for eav in self.browse(ids):
            res[eav.id] = eav.attribute.digits if eav.attribute else 2
        return res

    def on_change_with_digits(self, vals):
        attribute_obj = Pool().get('product.attribute')
        if vals.get('attribute'):
            attribute = attribute_obj.browse(vals['attribute'])
            return attribute.digits
        return 2

    def on_change_value_char(self, vals):
        res = {}
        if vals.get('value_char'):
            res['value'] = vals['value_char']
        return res

    def on_change_value_char_translate(self, vals):
        res = {}
        if vals.get('value_char_translate'):
            res['value'] = vals['value_char_translate']
        return res

    def on_change_value_number(self, vals):
        attribute_obj = Pool().get('product.attribute')
        res = {}
        if vals.get('value_number'):
            if vals.get('attribute'):
                attribute = attribute_obj.browse(vals['attribute'])
                res['value'] = self._format_number(attribute,
                    vals['value_number'])
            else:
                res['value'] = str(vals['value_number'])
        return res

    def on_change_value_select(self, vals):
        attribute_options_obj = Pool().get('product.attribute.option')
        res = {}
        if vals.get('value_select'):
            res['value'] = attribute_options_obj.read(
                vals['value_select'], ['name'])['name']
        return res

ProductEAV()


class Product(ModelSQL, ModelView):
    _name = 'product.product'

    attributes = fields.One2Many('product.eav', 'product', 'Attribute')

Product()
